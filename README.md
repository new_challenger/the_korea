<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

## Precautions for Clone

### Installing Composer Dependencies For Existing Applications

팀과 함께 어플리케이션을 개발하는 경우 Laravel 애플리케이션을 처음 만든 사람이 아닐 수 있습니다. 따라서 어플리케이션의 레포지토리를 로컬 컴퓨터에 복제한 후에는 

**Sail을 포함하여 어플리케이션의 Composer 의존성을 따로 설치해주어야합니다.**

어플리케이션(복사 또는 복제한)의 폴더로 이동하여 다음 명령어를 통해 설치 할 수 있습니다. 
```
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/var/www/html \
    -w /var/www/html \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
```

### 환경변수 내에 app_key를 발급받지 않으면, 페이지를 열 수 없습니다. 

**1.** 먼저 프로젝트 파일 내에, .env.example파일을 복제　→　(같은 path level) .env로 명명해준다.

**2.** .env안에, APP_KEY=가 공란으로 되어있음을 확인가능.

**3.** 터미널에　→ **./vendor/bin/sail php artisan key:generate** 를 입력해 APP＿KEY를 받급받을 수 있다. 

**이값은 함부로 갱신하면 안된다.**

**APP_KEY**를 변경하면 접속했던 사용자의 쿠키를 복호화할 수 없어 오류가 발생하기 때문.

### 설정된 포트로 로컬호스트 오픈하기
기본 포트는 80으로 설정되어 있으나, 다른 컨테이너에서 같은 포트를 사용하고 있을 확률이 높으므로 docker-compose.yml파일 내에서 포트 설정을 해주어야 합니다. 

**docker-compose.yml 내 포트관련부분**
```
        ports:
            - '${APP_PORT:-80}:80'
            - '${VITE_PORT:-5173}:${VITE_PORT:-5173}'
```
**Ex)**
```
        ports:
            - '8889:80'
            # - '${APP_PORT:-80}:80'
            - '${VITE_PORT:-5173}:${VITE_PORT:-5173}'
```

**./vendor/bin/sail up**　→　http://localhost:8889/

## Run scripts from package.json when files change(Design Hot_reload)

> ### Assuming that npm install command has already been executed

포트를 열어도 페이지가 보이지 않는다면, 현재 프로젝트 안에 작성된 스크립트 파일들이 컴파일 되고 있지 않다는 얘기!

새로운 터미널을 열어 밑에 커맨드를 동시에 실행시켜주자!

```
./vendor/bin/sail npm run dev
```







